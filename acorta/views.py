# Imports
from .models import Contenido
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader


@csrf_exempt
def index(request):
    # Maneja las solicitudes POST
    if request.method == "POST":
        # Recupera la URL y el identificador corto del formulario enviado
        url = request.POST['url']
        short = request.POST['short']

        # Asegura que la URL comience con http:// o https://, añadiendo https:// si no es así
        if not url.startswith('https://') and not url.startswith('https://'):
            url = "https://" + url

        # Intenta encontrar un objeto Contenido existente con el identificador corto y lo elimina si existe
        try:
            c = Contenido.objects.get(short=short)
            c.delete()
        except Contenido.DoesNotExist:
            # Si no existe, simplemente continúa (no hace nada)
            pass

        # Crea un nuevo objeto Contenido con la URL y el identificador corto proporcionados y lo guarda en la base de
        # datos
        c = Contenido(short=short, url=url)
        c.save()

    # Recupera todos los objetos Contenido de la base de datos
    content_list = Contenido.objects.all()
    # Carga la plantilla HTML para la página principal
    template = loader.get_template('acorta/index.html')
    # Crea un contexto con la lista de contenidos para pasar a la plantilla
    context = {'content_list': content_list}
    # Renderiza la plantilla con el contexto y devuelve la respuesta HTTP
    return HttpResponse(template.render(context, request))


# Vista para manejar la redirección basada en el identificador corto
def get_content(request, short):
    # Guarda el identificador corto en una variable local
    clave = short
    try:
        # Intenta obtener el objeto Contenido correspondiente al identificador corto
        contenido = Contenido.objects.get(short=clave)
        # Carga la plantilla de redirección
        template = loader.get_template('acorta/redirect.html')
        # Crea un contexto con el contenido obtenido
        context = {'content_list': contenido}
        # Renderiza la plantilla con el contexto y devuelve la respuesta HTTP
        return HttpResponse(template.render(context, request))
    except Contenido.DoesNotExist:
        # Si no se encuentra el Contenido, devuelve un error 404 con un mensaje
        return HttpResponse("Ha introducido la url acortada " + short + " la cual no está en la base de datos.",
                            status=404)
